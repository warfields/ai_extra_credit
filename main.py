#!/usr/bin/env python3.7
import sys
from Bnet import Bnet

# AI project 5

def main():
    args = sys.argv[1:]
    if len(args) > 6 or len(args) < 1:
        print("Improper Number of arguments")
        exit(-1) 

    b, e, a, j, m = [None] * 5

    truedict = {"t": True, "f" : False}

    for x in args:
        if x == "given":
            continue
        elif x[0] == "B":
            b = truedict[x[1]]
        elif x[0] == "E":
            e = truedict[x[1]]
        elif x[0] == "A":
            a = truedict[x[1]]
        elif x[0] == "J":
            j = truedict[x[1]]
        elif x[0] == "M":
            m = truedict[x[1]]
        else:
            print("INVALID INPUT")
            exit(-1)
    A_net = Bnet()

    if "given" in args:
        svars = args[:args.index("given")]
        givens = args[args.index("given") + 1:]
        for i in range(len(givens)):
            givens[i] = givens[i][0]

        probs = A_net.computeConditionalProbability(svars, givens,  b, e, a, j, m)

    elif len(args) == 5:
        probs = A_net.computeProbability(b,e,a,j,m)

    else:
        probs = A_net.computeSimpleJoint(args, b, e, a, j, m)

    print(f"Probability: {probs}")


# execute only if run as a script
if __name__ == "__main__":
    main()