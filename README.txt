Samuel Warfield 10731212
Python3.7

How the code is structured:

main.py is the file that takes the user input and then passes the information to the Bnet class

How to run the code, including very specific compilation instructions, if compilation is needed. Instructions such as "compile using g++" are NOT considered specific.
python3 main.py args