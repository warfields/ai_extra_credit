"""
The code should include and use a Bayesian network class. The class 
should include a member function called computeProbability(b, e, a, j, m), 
where each argument is a boolean, specifying if the corresponding event 
(burglary, earthquake, alarm, john-calls, mary-calls) is true or false. 
This function should return the joint probability of the five events.
"""
from itertools import product
from copy import deepcopy

p_b = .001
p_e = .002
p_a_be = {
    (True, True)   : .95,
    (True, False)  : .94,
    (False, True)  : .29,
    (False, False) : .001
}
p_j_a = {
    True  : .9,
    False : .05
}
p_m_a = {
    True  : .7,
    False :.01
}

class Bnet:
    
    # (burglary, earthquake, alarm, john-calls, mary-calls)
    def computeProbability(self, b, e, a, j, m):
        overall_prob = 1

        if b:
            overall_prob *=  p_b
        else:
            overall_prob *= 1 - p_b

        if e:
            overall_prob *= p_e
        else:
            overall_prob *= 1 - p_e

        if a:
            overall_prob *= p_a_be[(b,e)]
        else:
            overall_prob *= 1 - p_a_be[(b,e)]
        
        if j:
            overall_prob *= p_j_a[a]
        else:
            overall_prob *= 1 - p_j_a[a]

        if m:
            overall_prob *= p_m_a[a]
        else:
            overall_prob *= 1 - p_m_a[a]

        return overall_prob

    #P(xy | zq) = 
    def computeConditionalProbability(self, base, givens,  b, e, a, j, m):
        overall_prob = 1
        for x in base:
            overall_prob *= self.subConditional(x[0],givens, b, e, a, j, m)

        return overall_prob

    def subConditional(self, condition, givens, b, e, a, j, m):
        if condition == "J":
            if "A" in givens: # Fix ME
                return p_j_a[a]
            else:
                return p_j_a[True] * self.subConditional("A", givens, b, e, a, j, m) + p_j_a[False] * (1 - self.subConditional("A", givens, b, e, a, j, m))

        if condition == "M":
            if "A" in givens:
                return p_m_a[a]
            else:
                return p_m_a[True] * self.subConditional("A", givens, b, e, a, j, m) + p_m_a[False] * (1 - self.subConditional("A", givens, b, e, a, j, m))

        elif condition == "A":
            if "B" in givens and "E" in givens:
                return p_a_be[(b,e)]
            elif "B" in givens:
                return p_a_be[(b,True)] * p_e +  p_a_be[(b,False)] * (1 - p_e)
            elif "E" in givens:
                return p_a_be[(True, e)] * p_b +  p_a_be[(False,e)] * (1 - p_b)
            else:
                prob = 0
                prob += p_a_be[(True ,True)] * p_b * p_e
                prob += p_a_be[(True , False)] * p_b * (1 - p_e)
                prob += p_a_be[(False, True)] * (1 - p_b) * p_e
                prob += p_a_be[(False , False)] * (1 - p_b) * (1 - p_e)
                return prob

        elif condition == "B":
            return p_b
        else:
            return p_e


    def computeSimpleJoint(self, svars, b, e, a, j, m):
        overall_prob = 0
        testlist = [b,e,a,j,m]

        noneCount = 0
        noneLoc = []
        
        for i in range(len(testlist)):
            if testlist[i] is None:
                noneCount += 1
                noneLoc.extend([i])


        noneReplacements = list(product((True, False), repeat= noneCount))
        subprobs = []
        for i in range(len(noneReplacements)):
            subprobs.append(deepcopy(testlist))
            for y in range(noneCount):
                subprobs[i][noneLoc[y]] = noneReplacements[i][y]

        for x in subprobs:
            overall_prob += self.computeProbability(x[0],x[1],x[2],x[3],x[4])

        return overall_prob